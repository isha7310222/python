"""1.WAP to check numbers are divisible by 4 and 5
Input1: num1 =20
Output:
Numbers divisible by 4 and 5 is 20
Input1: num1 =15
Output: Numbers Not divisible by 4 and 5 is 15
"""

num=int(input("Enter number"))

if(num%4==0 and num%5==0):

    print("Number is divisible by 4 and 5 is %i"%num)
else:

    print("Number is not divisible by 4 and 5 is %i"%num)

