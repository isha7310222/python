'''
Program6 :
Rows = 5

# # # # #
@ @ @ @ @
# # # # #
@ @ @ @ @
# # # # #
'''

row=int(input("Enter row: "))

for i in range(1,row+1):
    for j in range(1,row+1):
        if(i%2==1):
            print("#",end=" ")
        else:
            print("@",end=" ")
    print()
