'''Program5 :
Row = 4
1 2 3 4
2 3 4 5
3 4 5 6
4 5 6 7
'''

row=int(input("Enter row: "))

for i in range(1,row+1):
    num=i;
    for j in range(row):
        print(num,end=" ")
        num=num+1
    print()
