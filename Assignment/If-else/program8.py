"""WAP to check if number is greater than 10 or not
i/p= 12
o/p= Yes 12 is greater than 10
"""

num=int(input("Enter a number"))

if(num>10):
    print("Yes",num,"is greater than 10");
else:
    print("No",num,"is less than 10")
