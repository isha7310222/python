"""WAP to take a number of month and print the number of days in that month
i/p=4
o/p= April is a 30 day-day month
"""

num=int(input("Enter number of months: "))

if(num==1):
    print("January is a 31 day-month")
elif(num==2):
    print("February is a 28-29 day-month");
elif(num==3):
    print("March is a 31 day-month");
elif(num==4):
    print("April is a 30 day-month");
elif(num==5):
    print("May is a 31 day-month");
elif(num==6):
    print("June is a 30 day-month");
elif(num==7):
    print("July is a 31 day-month");
elif(num==8):
    print("August is a 31 day-month");
elif(num==9):
    print("September is a 30 day-month");
elif(num==10):
    print("Octomber is a 31 day-month");
elif(num==11):
    print("November is a 30 day-month");
else:
    print("December is a 31 day-month");
