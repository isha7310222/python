"""WAP to check if number is divisible by5 or not
i/p=55
o/p= 55 is divisible by 5
"""

num=int(input("Enter number"))
if(num%5==0):
    print(num,"is divisible by 5")
else:
    print(num,"is not divisible by 5")
