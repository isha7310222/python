"""WAP to take integer from 0 to 6 and print corresponding weekdays
i/p=2
o/p=wednesday
"""

num=int(input("Enter number between 0 to 6: "))
if(num==0):
    print("Monday")
elif(num==1):
    print("Tuesday")
elif(num==2):
    print("Wednesday")
elif(num==3):
    print("Thursday")
elif(num==4):
    print("Friday")
elif(num==5):
    print("Saturday")
else:
    print("Sunday")
