
"""WAP to check if given character is vowel or consonant
i/p= 'a'
o/p= vowel
"""

ch=input("Enter a character: ")
if(ch=='a' or ch=='e' or ch=='i' or ch=='o' or ch=='u'):
    print("Vowel")
else:
    print("Character")
