'''Program 3:
WAP to print the sum of all numbers from a given range.
Input:
Start:1
End: 10
Output:
45
'''
Start=int(input("Enter a start: "))
End=int(input("Enter a end: "))

sum1=0
for i in range(Start,End):
    sum1=sum1+i

print(sum1)
