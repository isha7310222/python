'''Program 2:
Write a Program to print all Even numbers from a given range.
Input :
Start = 10;
End = 20;
Output:
10 12 14 16 18
'''

Start=int(input("Enter a start: "))
End=int(input("Enter a end: "))

for i in range(Start,End):
    if(i%2==0):
        print(i,end=" ")

print()
