'''Program 4:
WAP to print all the character values of the given ASCII value range from the
user
Input :
Enter the start of range : 1
Enter end of range: -2
Output :
Wrong input
Input :
Enter start of range : 65
Enter end of range : 67
Output :
The character of ASCII value 65 is A.
The character of ASCII value 66 is B.
The character of ASCII value 67 is C.
.
.
.
The character of ASCII value 89 is Y.
'''
Start=int(input("Enter a start: "))
End=int(input("Enter a end: "))

if(Start>=65 and Start<=89 and End>=65 and End<=91):
    pass
else:
    print("Wrong input")

for i in range(Start,End+1):
   print("The character of ASCII value",i,"is",chr(i)) 

print()
