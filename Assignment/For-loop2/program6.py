'''Program 6:
WAP to print all the ASCII values from a given character range.
Input :
Enter the start of range : A
Enter end of range: Z
Output :
ASCII value of A is 65
ASCII value of B is 66
ASCII value of C is 67
.
.
.
ASCII value of Y is 89
'''

start_char = input("Enter the start of range: ")
end_char = input("Enter the end of range: ")

start_ascii = ord(start_char)
end_ascii = ord(end_char)

if start_ascii > end_ascii:
    print("Start should be less than or equal to the end.")
else:
    for i in range(start_ascii, end_ascii + 1):
        print("ASCII value of", chr(i)," is ", i)
