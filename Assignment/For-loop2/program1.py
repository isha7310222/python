'''Program 1:
WAP to print numbers from a given range.
Input :
Start = 100;
End = 110;
Output:
100 101 103 104 105 106 107 108 109
'''

Start=int(input("Enter a start: "))
End=int(input("Enter a end: "))

for i in range(Start,End):
    print(i,end=" ")

print()
