'''
Program 10:
WAP program to calculate and print the product of the count of odd
numbers within a given range.
Input:
Start: 1
End: 11
Output:
10395
'''
Start=int(input("Enter a start: "))
End=int(input("Enter a end: "))

prod=1
for i in range(Start,End+1):
    if(i%2==1):
        prod=prod*i

print(prod)
