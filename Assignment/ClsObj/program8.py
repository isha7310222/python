'''8. Write a realtime example of oop which contains the following point.
a. Instance variable
b. constructor
c. Class variable
d. Instance method
'''

class BankAccount:
    interest_rate = 4.5  

    def __init__(self, account_number, balance=0):
        self.account_number = account_number
        self.balance = balance

    def deposit(self, amount):
        self.balance += amount
        print(f"Deposit of ${amount} successful..Current balance: ${self.balance}")

    def withdraw(self, amount):
        if self.balance >= amount:
            self.balance -= amount
            print(f"Withdrawal of ${amount} successful...Current balance: ${self.balance}")
        else:
            print("Insufficient funds")

    @classmethod
    def set_interest_rate(cls, rate):
        cls.interest_rate = rate
        print(f"Interest rate set to {rate}%")

    def calculate_interest(self):
        interest_amount = (self.balance * self.interest_rate) / 100
        print(f"Interest earned: ${interest_amount}")


account1 = BankAccount("12345", 1000)
account2 = BankAccount("67890", 5000)


account1.deposit(500)
account2.withdraw(1000)
account1.calculate_interest()

BankAccount.set_interest_rate(5.0)


