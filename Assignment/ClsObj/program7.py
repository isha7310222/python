'''WAP defines a class Sum with a mySum method that returns the sum of two numbers.
The program creates two class objects, takes user input to set values for each object,

prints the return sum for both objects, and then determines whether the total sum is even
or odd. '''

class Sum():

    def __init__(self,n1,n2):

        self.n1=n1
        self.n2=n2

    def mysum(self):

        return (self.n1 + self.n2)

n1=int(input("Enter first number for object 1: "))
n2=int(input("Enter second number for object 1: "))
obj1=Sum(n1,n2)
ret1=obj1.mysum()


n3=int(input("Enter first number for object 2: "))
n4=int(input("Enter second number for object 2: "))
obj2=Sum(n1,n2)
ret2=obj2.mysum()


total_sum=ret1+ret2

if(total_sum%2==0):

    print("Total sum is even")

else:
    print("Total sum is odd")

