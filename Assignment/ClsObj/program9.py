'''9. Write a real-time example of OOP on IPL cricket by the following point.
a. Instance variable
b. constructor
c. Class variable
d. Instance method'''

class IPL:

    Stadium="Wankhede"

    def __init__(self,T_name,captain):

        self.T_name=T_name;
        self.captain=captain;

    def TeamInfo(self):

        print(self.T_name)
        print(self.captain)


T1=IPL("CSK","MSD")
T2=IPL("RCB","Viarat")

T1.TeamInfo()
T2.TeamInfo()
