'''Python program that defines a parent function with two nested functions
(myIndex and myPalindrome). The program prompts the user to choose between
these two functions and then calls the selected function based on the user's
input'''

def Parent():

    def myIndex(lst,ele):
        if ele in lst:
            return lst.index(ele)

        else:
            return -1

    def palindrome(s):
        return s==s[-1::-1]

    return myIndex,palindrome


indexFun,palindromeFun=Parent()


def switch(choice):

    if(choice==1):
        elements=input("Enter list of elements separated by spaces: ").split()

        target=input("Enter target element to find its index: ")
        
        result=indexFun(elements,target)
        
        if result!=-1:
            print(f"The index of {target} in the list is : {target}")
        else:
             print("The target not found in list")

    
    elif(choice==2):

        word=input("Enter a word to check if palindrome: ")
    
        if palindromeFun(word):
            print(f"{word} is palindrome")

        else:
            print(f"{word} is not palindrome")

    else:
        print("Invalid choice")


print("Choose function")
print("1.Find index of element in list")
print("2.check palindrome")


print("Enter your choice 1 or 2")
choice=int(input())

switch(choice)
