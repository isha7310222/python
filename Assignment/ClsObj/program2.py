
def outer():

    def inner():

        return "In inner function!"

    return inner


retObj=outer()
retInner=retObj()

print(retInner)
