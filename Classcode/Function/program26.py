#Nested Function
#Function should must have body before calling
def outer():
    def inner1():
        print("In Inner1 function")
    inner2()#UnboundLocalError: local variable 'inner2' referenced before assignment
   

    def inner2():
        print("In Inner2 function")


    print("In outer function")
outer()
