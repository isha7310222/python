#Function returning function

def outer(x,y):

    def inner(a,b):
        print("Inner fun")
        return a+b

    print("In outer fun")
    print(x+y)
    return inner

retObj=outer(10,20)
intRet=retObj(3,4)
print(retObj)
