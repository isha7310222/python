#Nested Function

def outer():
    def inner():
        print("In Inner function")
    
    print("In outer function")
    inner()

outer()
