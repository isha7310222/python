
#Named argument in function

def compInfo(compName,empcount):
    print(compName,":",empcount)

compInfo("Microsoft",70000)
compInfo("Amazon",50000)
compInfo("Apple",30000)
compInfo(10000,"Google")

compInfo(empcount=1000,compName="eQ Technologic")

