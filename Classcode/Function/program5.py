# Return statement
# factorial of number using function

def fact(num):
    fact1 = 1
    for i in range(1, num + 1):
        fact1 = fact1 * i

    return fact1

x = int(input("Enter x: "))
factorial = fact(x)
print("Factorial:", factorial)






