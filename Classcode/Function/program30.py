#Function returning function

def outer(x,y):
    
    def inner():
        print("Inner fun")
    
    print("In outer fun")
    return inner

retObj=outer(10,20)
retObj()
