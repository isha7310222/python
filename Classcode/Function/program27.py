#Nested Function
def outer():
    def inner1():
        print("In Inner1 function")


    def inner2():
        print("In Inner2 function")


    print("In outer function")
outer().inner1()#AttributeError: 'NoneType' object has no attribute 'inner1'
