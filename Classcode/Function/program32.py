
def outer():
    def inner1():
        print("In inner1 fun")

    def inner2():
        print("In inner2 fun")

    return inner1,inner2

#method1
inn1,inn2=outer()
inn1()
inn2()

#method2
retObj=outer()

for i in retObj:
    i()

