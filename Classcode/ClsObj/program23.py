


class Employee:

    compName="Facebook"

    def __init__(self):

        print("In constructor")

        self.empId=12
        self.empName="Kanha"


    def empInfo(self):
        print(self.empId)
        print(self.empName)


emp1=Employee()
emp2=Employee()

emp1.empInfo()
emp2.empInfo()

emp1.empId=50
empName="Badhe"

emp1.empInfo()
emp2.empInfo()

emp1.empName="Badhe"

emp1.empInfo()
emp2.empInfo()

print(emp1.empId)
print(emp1.empName)
print(emp2.empId)
print(emp2.empName)

#AttributeError: type object 'Employee' has no attribute 'empId'
#print(Employee.empId)
#print(Employee.empName)
