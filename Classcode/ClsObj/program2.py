
class Employee:

    name="kanha"
    empId=22
    sal=1.5

    def work():
        print("FrontEnd Dev")

obj=Employee()
#work()
#NameError:work not defines


class Employee:

    name="kanha"
    empId=22
    sal=1.5

    def work():
        print("FrontEnd Dev")

obj=Employee()
#obj.work()
#TypeError: Employee.work() takes 0 positional arguments but 1 was given


#self required

class Employee:

    name="kanha"
    empId=22
    sal=1.5

    def work(self):
        print("FrontEnd Dev")

obj=Employee()
obj.work()
