
#NameError and AttributeError

class Employee:

    def __init__(self):

        print("In constructor")
        self.empId=12
        self.empName="Kanha"

    def printdata(self):

        print(self.empId)
        print(self.empName)


emp1=Employee()
emp2=Employee()
run()
#NameError: name 'run' is not defined
emp1.run()
#AttributeError: 'Employee' object has no attribute 'run'
