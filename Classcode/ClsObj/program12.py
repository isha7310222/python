
#type as metadata

class MyClass:
    pass

print(type(MyClass))



#metadata and metaclasses

class myMetaClass(type):

    def __new__(cls,name,bases,dct):

        dct['meta_data']='some metadata'
        return super().__new__(cls,name,bases,dct)


class MyClass(metaclass=myMetaClass):

    pass

print(MyClass.meta_data)


