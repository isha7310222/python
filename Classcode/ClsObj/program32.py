
#static method

class Player:

    teamName="India"

    def __init__(self,jerNo,pName):

        print("In constructor")
        self.jerNo=jerNo
        self.pName=pName

    @staticmethod

    def playerInfo():

        print(Player.teamName)


cric1=Player(18,"Virat")
cric2=Player(45,"Rohit")

Player.playerInfo()
cric1.playerInfo()
