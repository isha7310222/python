

class Demo:

    def __new__(self):

        print("Memory allocate")
        return super.__new__(self)

    def __init__(self):

        print("In init")
        

#obj=Demo()
#TypeError: super.__new__(Demo): Demo is not a subtype of super




class Demo:

    def __new__(self):

        print("Memory allocate")
        return object.__new__(self)

    def __init__(self):

        print("In init")

obj=Demo()
