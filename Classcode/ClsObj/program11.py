
#object address

class Company(object):

    def disp():

        print("Hello")


obj=Company()
print(id(obj))

#Functions are objects

def my_function():

    print("Hello from my-function")

obj=my_function
obj() #call fun through variable


#class is also object

class MyClass:
    pass

obj=MyClass()

print(type(obj))
