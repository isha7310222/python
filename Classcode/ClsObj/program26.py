


class Company():

    compName="Facebook"

    def __init__(self):

        print("In constructor")

        self.empId=12
        self.empName="Kanha"


    def empInfo(self):
        print(self.empId)
        print(self.empName)
        print(self.compName)

emp1=Company()
emp2=Company()

emp1.empInfo()
emp2.empInfo()

emp1.compName="Meta"

emp1.empInfo()
emp2.empInfo()

Company.compName="Meta.."

emp1.empInfo()
emp2.empInfo()
