
#methods
#instance method

class Player:

    teamName="India"

    def __init__(self,jerNo,pName):

        print("In constructor")
        self.jerNo=jerNo
        self.pName=pName


    def playerInfo(self):

        print(self.jerNo)
        print(self.pName)
        print(self.teamName)


cric1=Player(18,"Virat")
cric2=Player(45,"Rohit")

cric1.playerInfo()
cric2.playerInfo()

Player.teamName="Bharat"

cric1.playerInfo()
cric2.playerInfo()

Player.playerInfo(cric1)
#Player.playerInfo()
#TypeError: Player.playerInfo() missing 1 required positional argument: 'self'
