
# x,y,z=input("Enter values:")
#ValueError: too many values to unpack (expected 3)

x,y,z=[int(data) for data in input("Enter values: ").split(" ")]

print(type(x))
print(y)
print(z)
print(x+y+z)


x,y,z=[(data) for data in input("Enter values: ").split(" ")]

print(type(x))
print(y)
print(z)
print(x+y+z)
