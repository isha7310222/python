
def outerFun():
    print("In outer fun")

    def innerFun1():
        print("In inner1")

    def innerFun2():
        print("In inner2")

    return innerFun1,innerFun2

ret=outerFun()

for i in ret:
    i()

ret[0]()
ret[1]()
