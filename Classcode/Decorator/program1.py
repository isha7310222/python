
def outerFun():
    print("In outer fun")

    def innerFun1():
        print("In inner1")

    def innerFun2():
        print("In inner2")

    innerFun1()
    innerFun2()

outerFun()
