
#Decorator
def decorFun(func):
    print("In decorFun")

    def wrapper(*args):
        print("start wrapper")
        val=func(*args)
        print("End wrapper")
        return val
    return wrapper

@decorFun

def normalFun(x,y):
    print("In normal fun")
    return x+y


#normalFun=decorFun(normalFun)
print(normalFun(10,20))
