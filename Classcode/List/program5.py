#Deletion methods in list
#1.remove 2.pop 3.clear

player=["Rohit","Shubhman","virat","Shreyas","KLRahul"]
print(player)

player.remove("Shreyas")
print(player)

#player.remove(5)...valueError

player.pop()
print(player)

player.pop(2)
print(player)


#player.pop(9)....IndexError

player.clear()
print(player)
