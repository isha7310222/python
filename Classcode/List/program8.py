
#list is by default deep copy

player=["Virat","Rohit","Sheryas"]
newlist=player.copy()

player[1]="MSD"

print(player)
print(newlist)


#deepcopy
import copy as cp

lang=["cpp","Java","Python",["Go","Rust","Dart"]]
newlist=cp.deepcopy(lang)

lang[3][1]="JS"
print(lang)
print(newlist)
