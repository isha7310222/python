
#Accessing elements from list

#index

player=["Rohit","Shubhman","virat","Sheyas","KLRahul"]

print(player[0])
print(player[2])
print(player[4])
#print(player[5])#IndexError:List index out of range


#slicing

print(player[2::])
print(player[2:4:2])
print(player[:5:3])
print(player[::3])
print(player[4:2:2])
print(player[4:2:-1])
print(player[-1:-5:])
print(player[-2:-4:-2])
print(player[-1::-1])
