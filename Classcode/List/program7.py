#Nested List

lang=["cpp","Java","Python",["Go","Rust","Dart"]]

print(lang[1])
print(lang[3])
print(lang[3][1])


#Shallow copy

newlist=lang.copy()
print(lang)
print(newlist)
lang[3][1]="JS"

print(lang)
print(newlist)

lang[0]="C"

print(lang)
print(newlist)
print(id(lang)==id(newlist))

