#Comprehensive type

normList=[num for num in range(1,11)]
print(normList)
print(type(normList))



sqr=[num*num for num in range(1,11)]
print(sqr)
print(type(sqr))


even_list=[num*num for num in range(1,11) if num%2==0]
print(even_list)
print(type(even_list))


odd_list=[num*num for num in range(1,11) if num%2==1]
print(odd_list)
print(type(odd_list))

