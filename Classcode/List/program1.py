
#List
#we can add tuple,set,dictionary in list
employee=[{10,"Ashish",1.5},{11,"Kanha",1.75}]
print(employee)
print(type(employee))


#List Constructor
#iterable parameters
player=list()
print(player)
print(type(player))

player=list([10,20,30,40])
print(player)


#List as parameter
listdata=[10,50,40]
batsman=list(listdata)
print(batsman)
print(type(batsman))


#set as parameter
listdata={10,50,40}
batsman=list(listdata)
print(batsman)
print(type(batsman))


#cananot give dictionary in set coz it is unhashable
listdata={{10:40},{20:50},{30:60}}#error
batsman=list(listdata)
print(batsman)
print(type(batsman))
