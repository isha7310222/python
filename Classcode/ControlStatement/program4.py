# if-elif-else statement

num=int(input("Enter a number"))
if(num>0):
    print("{} is positive".format(num))
elif(num<0):
    print("{} is negative".format(num))
else:
    print("{} is zero".format(num))
