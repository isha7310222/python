''' 1  2  3  4  5
    6  7  8  9  10
    12 18 20 21 24
    27 30 36 40 42
    45 48 50 54 60 '''

num = 1
row = 5

for i in range(1, row + 1):
    for j in range(1, row + 1):
        temp = num
        sum = 0

        while temp != 0:
            r = temp % 10
            sum = sum + r
            temp = temp // 10

        if num % sum == 0:
            print(num, end=" ")
        else:
            j -= 1

        num += 1

    print()
