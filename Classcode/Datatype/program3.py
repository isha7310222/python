
#Sequence Type
#String Type
#way-1

name1="Kanha"
print(name1)
print(type(name1))

#way-2

name2='Ashish'
print(name2)
print(type(name2))

#way-3

name3='''Rahul'''
print(name3)
print(type(name3))

#way-4

name4="""Isha"""
print(name4)
print(type(name4))

#way-5

name5='A'
print(name5)
print(type(name5))
