str1="Core2web"

#32.replace
print(str1.replace('r','i')) #Coie2web

#33.rindex

print(str1.rindex('e')) #6

#34.split
str2="core2web,icub,biencaps"
print(str2.split(",")) #['core2web', 'icub', 'biencaps']

str3="core2web icub biencaps"

print(str3.split(",")) #['core2web icub biencaps']

#to take input in a single line
#if input is not comma separted then error valueError
x,y,z=[int(val) for val in input("enter number: ").split(",")]
print(x+y+z)

#35.startswith

print(str1.startswith("Cor")) #True

#36.swapcase

print(str1.swapcase())  #cORE2WEB

#37.title
print(str1.title()) #Core2web

#38.removeprefix
print(str1.removeprefix("Cor")) #e2web
 
#39.removesuffix
print(str1.removesuffix("e2")) #Core2web
print(str1.removesuffix("web")) #Core2


#40.zfill
print(str1.zfill(12)) #0000Core2web

#41.translate

print(str1.translate([1,2,3])) #Core2web

