
str1="def"

#23.isidentifier.....keyword

print(str1.isidentifier()) #True

#24.join

str2="Core2web"
print("-".join(str2))  #C-o-r-e-2-w-e-b

str3=["core2web","incubator","Biencaps"]
print("-".join(str3))  #core2web-incubator-Biencaps

#25.ljust

print(str2.ljust(12,"#")) #Core2web####

#26.rjust

print(str2.rjust(12,"#")) #  ####Core2web

#27.strip

str4="    Core2web   "
print(str4.strip()) #Core2web

#28.lstrip
print(str4.lstrip()) #Core2web

#29.rstrip
print(str4.rstrip()) #    Core2web

#30.partition

str10="core2webincubator"

print(str10.partition("web")) #('core2', 'web', 'incubator')

print(str10.partition("hash")) #('core2webincubator', '', '') 

#31.rpartition
print(str10.rpartition("web"))  #('core2', 'web', 'incubator')






