
str1="Core2web"

#10.index
print(str1.index('r')) #2


#11.isalnum

print(str1.isalnum()) #True

#12.isalpha

str2="ISha"
print(str2.isalpha())  #True


#13.encode
str3="234"
encoded=str3.encode()
print(encoded) #b'Core2web' 


#14.isascii

print(str1.isascii())  #True
print(encoded.isascii()) #True

#15.isdigit()

str9="123"
str10="0123"
str11="ox123"

print(str9.isdigit())  #True
print(str10.isdigit()) #True
print(str11.isdigit()) #False

#16.isdecimal

print(str9.isdecimal()) #True
print(str10.isdecimal()) #True
print(str11.isdecimal()) #False

#17.islower

str5="Isha"
print(str5.islower()) #False

#18.isupper
str6="ISHA"
print(str5.isupper()) #False
print(str6.isupper()) #True

#19.isnumeric

str7="1020"
print(str7.isnumeric())

#20.isprintable

str8="welcome\t to \nPune"
print(str8.isprintable()) #False
print(str7.isprintable()) #True

#21.isspace

str9="Welcome to Python"
print(str9.isspace()) #True
print(str6.isspace()) #False

#22.istitle

print(str5.istitle()) #True
print(str8.istitle()) #False


