
#methods

str1="Core2web Incubator"

#1.capitalize
print(str1.capitalize()) #Core2web incubator

#2.casefold
str2="core2web Incubator"

print(str1==str2)  #False
print(str1.casefold()==str2.casefold())  #True

#3.center

str3="Isha"
print(str3.center(7,"*"))  #**Isha*

#4.count

print(str2.count('e',2))  #2

#5.endswith

print(str1.endswith("tor")) #True

#6.expandtabs  ...reduce tabs

strr="core2web \tincubator"
print(strr)                 #core2web        incubator
print(strr.expandtabs(19))  #core2web           incubator


