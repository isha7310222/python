
#type1
str1='core2web'
print(str1)
print(type(str1))
print(len(str1))


#type2

str2="incubator"
print(str2)
print(type(str2))
print(len(str2))

#type3

str3='''Biencaps
system
pvt
lmt'''
print(str3)
print(type(str3))
print(len(str3))

# str4="incubator ...SyntaxError: unterminated string literal
# by core2web"

#highlight

str9="Welcome to 'IIT' Narhe"
print(str9)

# str10="Welcome to "IIT" Narhe"....SyntaxError

str10='Welcome to "IIT" Narhe'
print(str10)

