
#Access

#1.index

str1="core2web"

print(str1[3])
print(str1[4])
print(str1[-2])
print(str1[-5])


#2.slicing

print(str1[ : :2])        #cr2e
print(str1[ 2: 5:])       #re2
print(str1[ -2:-5 :-1])   #ew2
print(str1[ -3: :-1])     #w2eroc
print(str1[ 2: 5:-1])     # no o/p


str1="Core2web"
str2="Core2web"

print(str1==str2)  #True


str3="Core2web"
str4="core2web"

print(str3==str4)
print(str3<str4)
