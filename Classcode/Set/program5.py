
setdata1={1,2,3,4}
setdata2={3,4,5,6}

#symmentric_difference

setdata3=setdata1.symmetric_difference(setdata2)
print(setdata3)  #{1,2,5,6}

#symmentric_difference_update

setdata1.symmetric_difference_update(setdata2)
print(setdata1)  #{1,2,5,6}

#union

setdata4=setdata1.union(setdata2)

print(setdata4) #{1, 2, 3, 4, 5, 6} 

#update

setdata1.update(setdata2)
print(setdata1)  #{1, 2, 3, 4, 5, 6}
