
#Methods in set

setdata1={1,2,3,4,5}

#add

setdata1.add(6)
print(setdata1) #{1, 2, 3, 4, 5, 6}

#copy

setdata2=setdata1.copy()

print(setdata1)  #{1, 2, 3, 4, 5, 6}
print(setdata2)  #{1, 2, 3, 4, 5, 6}


#difference

dataset1={1,2,3,4}
dataset2={3,4,5,6}

setdata3=dataset1.difference(dataset2)
print(setdata3) #{1,2}

setdata4=dataset1-dataset2
print(setdata4)  #{1,2}

#difference_update

dataset1.difference_update(dataset2)
print(dataset1)  #{1,2}
print(dataset2)  #{3,4,5,6}

#discard

setdata1.discard(3)
print(setdata1)  #{1,2,4,5,6}
