
dataset1={1,2,3,4,5}

# print(dataset[2])....not subscriptable

dataset1.add(6)
print(dataset1)

#frozen set immutable
setdata2=frozenset([1,2,3,4,5])
print(setdata2)

setdata2.add(6) #AttributeError:frozenset objrct has no attribute add


