
setdata1={1,2,3,4}
setdata2={3,4,5,6}

#intersection

setdata3=setdata1.intersection(setdata2)
print(setdata3)  #{3,4}

#intersection_update
setdata1.intersection_update(setdata2)
print(setdata1) #{3,4}


#isdisjoint

print(setdata1.isdisjoint(setdata2)) #False

#issubset

dataset1={1,2,3,4}
dataset2={3,4,5,6,1,2}

print(dataset1.issubset(dataset2))  #True

#issuperset

print(dataset1.issuperset(dataset2)) #False
