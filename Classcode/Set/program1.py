
#Set

#unique values only
setdata1={10,10,10}
print(setdata1)


setdata={10,20,30,40}
print(setdata)


#empty curly brace is a dictionary
setdata2={}
print(setdata2)
print(type(setdata2))


#set type
setdata3={10}
print(type(setdata3))


#constructor of set
setdata4=set()
print(setdata4)
print(type(setdata4))

#remain same sequence

setdata5={1,2,3,4,5}
print(setdata5)

#True and 1 same no duplicates allowed
setdata6={1,"A",105,True,False}
print(setdata6)


#set of strings 
setdata7={"Isha","Mahima","Ayush"}
print(setdata7)


#pass list to set constructor

setdata8=set([10,20,30,40])
print(setdata8)


#cannot pass more than one data to set constructor
# setdata9=set(10,20,30)...TypeError: set expected at most 1 argument, got 3

#set constructor take only iterable and empty

# setdata9=set(10).........TypeError: 'int' object is not iterable

setdata10=set("shashi")
print(setdata10)



