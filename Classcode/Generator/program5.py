def fun():
    print("start fun")
    yield 10
    yield 20
    yield 30
    print("End fun")

ret=fun()

print(next(ret))
print(next(ret))
print(next(ret))
# print(next(ret)) ...Error :stopIteration


# write empty yield at the end to ensure all statements of normal fun must run
def run():
    print("start run")
    yield 10
    yield 20
    yield 30
    print("End run")
    yield
ret=run()

print(next(ret))
print(next(ret))
print(next(ret))
next(ret)
