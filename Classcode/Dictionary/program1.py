#Dictionary literal

player={45:"Rohit",18:"Virat",1:"KLRahul"}
print(player)
#Dictionary Constructor

player1=dict({45:"Rohit",18:"Virat",1:"KLRahul"})
print(player1)
# player2=((45:"Rohit",18:"Virat",1:"KLRahul")).....InvalidSyntax

#mixtype

myInfo={"name":"Isha","CompanyName":"eQTechnologic","salary":12.5,"siblings":["Mahima","Ayush"]}
print(myInfo)

#Nested Dictionary

player3={45:"Rohit",18:"Virat",1:"KLRahul","Next to bat":{96:"sheyas",63:"SKY",8:"Jaddu"},77:"xyz"}
print(player3)

print(player3[18])
#print(player3[33])....KeyError
print(player3["Next to bat"][63])


#mutable
player3[100]="virat"
print(player3)
