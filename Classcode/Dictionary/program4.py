#other methods in dictionary

#1.copy  2.update  3.setdefault  4.fromkeys

#deepcopy 
#shallow copy only for nested dictionary
player={45:"Rohit",18:"Virat",1:"KLRahul"}
newData=player.copy()

print(player)
print(newData)

player[18]="Kohli"

print(player)
print(newData)


newSet={63:"sky",33:"Hardik"}
player.update(newSet)
print(player)



val=player.setdefault(45,"Sharma")
print(val)
print(player)

val=player.setdefault(15,"Sharma")
print(val)
print(player)


lang=["JS","Java","Python","Springboot"]
teacher="Shashi"

print(player.fromkeys(lang))
print(player.fromkeys(lang,teacher))

