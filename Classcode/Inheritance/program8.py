
class Parent:

    z=30

    def __init__(self):

        print("In parent constructor")
        self.x=10
        self.y=20

    def printData(self):

        print(self.x)
        print(self.y)

    @classmethod
    def dispParent(cls):

        print(cls.z)


class Child(Parent):

    pass


obj=Child()
obj.printData()
obj.dispParent()
print(id(obj.dispParent))
print(id(Parent.dispParent))
