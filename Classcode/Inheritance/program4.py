
#constructors must be with matching argument

class Demo:

    def __init__(self):

        print("In constructor1")


    def __init__(self,x):

        print("In constructor 2")

#TypeError: Demo.__init__() missing 1 required positional argument: 'x'

obj1=Demo()

