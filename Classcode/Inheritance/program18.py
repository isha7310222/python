
class Demo:

    def __init__(self):

        print("In constructor")

    def __del__(self):

        print("In destructor")


def fun():

    print("In fun")
    obj=Demo()
    print("End fun")


print("start code")
fun()
print("End code")
