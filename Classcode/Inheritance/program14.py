
#class variable and class method

class Parent:

    x=10
    y=20

    @classmethod
    def dispData(cls):
        
        print("In parent method")
        print(cls.x)
        print(cls.y)


class Child(Parent):

    x=30
    y=40

    @classmethod
    def dispData(cls):

        print("In child method")
        print(cls.x)
        print(cls.y)
        super().dispData()



obj1=Child()
obj1.dispData()
