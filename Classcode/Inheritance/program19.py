

class Demo:

    def __init__(self):

        print("In constructor")

    def __del__(self):

        print("In destructor")


def fun():

    print("In fun")
    obj=Demo()
    print("End fun")
    return obj

print("start code")
retobj=fun()
print("End code")
