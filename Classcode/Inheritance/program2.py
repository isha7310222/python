#instance variable

class Demo:

    def __init__(self):
        
        print("In constructor")
        self.x=10
        self.y=20


    def setData(self,z):

        self.z=30


    def printData(self):
        print(self.x) 
        print(self.y)
       #print(self.z)...AttributeError: 'Demo' object has no attribute 'z'


obj1=Demo()
obj1.printData()

