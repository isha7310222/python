

def fun():
    print("In fun")


class Parent:

    fun()
    print("start class")

    def __init__(self):

        print("In constructor")

    print("End class")


print("Start code")

obj=Parent()

print("End code")
