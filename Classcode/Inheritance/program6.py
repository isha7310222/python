


class Parent:

    def __init__(self):

        print("In parent constructor")

    def parentFun(self):

        print("In parent function")


class Child(Parent):

    def __init__(self):

        print(Parent()) #it will create parent object
        print(super().__init__())
        print(Parent.__init__(self))  #without creating object
        print("In child constructor")


    def childFun(self):

        print("In child function")


obj1=Child()
obj1.parentFun()
obj1.childFun()

