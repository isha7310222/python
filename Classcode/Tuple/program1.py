
lang=("cpp","Java","Python","Go","Rust","Dart",10,10.5)

print(lang)
print(type(lang))
print(lang[4])


#Nested tuple
lang=("cpp","Java","Python",("Go","Rust","Dart"))
print(lang[3][1])

#immutable
#lang[3]="JS"...TypeError


#methods
#1.index 2.count

print(lang.index("Java"))
print(lang.count("Java"))
