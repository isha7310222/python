
#Unary operator

x=10
print(x)

ans=-x
print(ans)

#Ignore preIncrement
ans=++x
print(ans)

#error:Invalid syntax
#ans=x++
print(ans)


x=10
y=20
ans=x++y #x + +y
print(ans)
