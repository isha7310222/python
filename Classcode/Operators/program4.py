
#Logical Operator

x=True
y=False

print(x and y)
print(x or y)
print(not x)

a=10
b=20
print(a and b)
print(a or b)
print(not a)


p=0
q=20
print(p and q)
print(p or q)
print(not q)
