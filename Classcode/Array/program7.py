#Array slicing

import array as arr
arrdata=arr.array('i',[10,20,30,40,50])

for i in arrdata:
    print(i)

print("2")

for i in arrdata[1:4:]:
    print(i)

print("3")

for i in arrdata[1:4:-1]:
    print(i)

print("4")

for i in arrdata[4:1:-1]:
    print(i)

print("5")


for i in arrdata[4::1]:
    print(i)

print("6")


for i in arrdata[4:6:1]:
    print(i)

print("7")

print(arrdata[4])

# print(arrdata[5])....error : array index out of range
