#Array Functions

import array as arr

data=arr.array('i',[10,20,30,30,40])

#1] append()
data.append(60)
print(data)

#2]buffer_info()
print(data.buffer_info())

print(id(data))

#3]count(arg)

print(data.count(30))

#4]extend()
data.extend([70,80])
print(data)

#5]fromList()

listdata=[10,20,30,40];
arrdata=arr.array('i',[100,200,300,400])

arrdata.fromlist(listdata)
print(arrdata)

#6]index()

print(arrdata.index(400))

#7]insert()

arrdata.insert(4,500)
print(arrdata)

#8]pop()...remove last data

arrdata.pop()

print(arrdata)

#9]remove()

arrdata.remove(100)
print(arrdata)

#10]reverse()

arrdata.reverse()
print(arrdata)

#11]tolist()

copylist=arrdata.tolist()
print(arrdata)
print(copylist)

